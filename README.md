# xtt
Xorg time tracking

For my own reference, I need to know how much time I spent at work on my PC. Here's a simple shell script that records what I'm doing and how long I'm idling every once in a while. These records can be then processed with your favorite reporting tool.

## usage

Just clone the repo and run bin/xtt. Time tracking records will be recorded in TSV format. It will include current time, watch interval, time iddle and the x-window title active at the time.

## dependencies
sh
xprop
xprintidle
tail
rev
cut
date
watch

